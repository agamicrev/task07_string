package tasks;

/**
 * Знайти таке слово у першому реченні, якого немає ні в
 * одному з інших речень.
 */
public class Task3 {
  private Text text = new Text();

  public void taskThree() {
    text.findWordInFirstSentence();
  }

  public static void main(String[] args) {
    Task3 output = new Task3();
    output.taskThree();
  }
}
