package tasks;

/**
 * Надрукувати слова тексту в алфавітному порядку по першій
 * букві. Слова, що починаються з нової букви, друкувати з
 * абзацного відступу.
 */
public class Task6 {

    private Text text = new Text();

    public void taskSix() {
        text.printWordInAlphabetOrder();
    }

    public static void main(String[] args) {
        Task6 output = new Task6();
        output.taskSix();
    }

}
