package tasks;

/**
 * Є текст і список слів. Для кожного слова з заданого
 * списку знайти, скільки разів воно зустрічається у кожному
 * реченні, і відсортувати слова за спаданням загальної
 * кількості входжень.
 */
public class Task10 {

    private Text text = new Text();

    public void taskTen() {
        text.findRepeatOfWords();
    }

    public static void main(String[] args) {
        Task10 output = new Task10();
        output.taskTen();
    }

}
