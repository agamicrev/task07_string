package tasks;

/**
 * Слова тексту, що починаються з голосних букв,
 * відсортувати в алфавітному порядку по першій
 * приголосній букві слова.
 */
public class Task8 {
    private Text text = new Text();

    public void taskEight() {
        text.sortWordAfterFirstConsonant();
    }

    public static void main(String[] args) {
        Task8 output = new Task8();
        output.taskEight();
    }
}
