package tasks;

/**
 * У певному реченні тексту слова заданої довжини замінити
 * вказаним підрядком, довжина якого може не співпадати з
 * довжиною слова.
 */
public class Task16 {

  private Text text = new Text();

  public void taskSixteen() {
    text.replaceWords();
  }

  public static void main(String[] args) {
    Task16 output = new Task16();
    output.taskSixteen();
  }
}
