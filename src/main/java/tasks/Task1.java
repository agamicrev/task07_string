package tasks;


/**
 * Знайти найбільшу кількість речень тексту, в яких є
 * однакові слова.
 */
public class Task1 {
  private Text text = new Text();

  public void taskOne() {
    text.findMaxAmount();
  }

  public static void main(String[] args) {
    Task1 output = new Task1();
    output.taskOne();
  }
}
