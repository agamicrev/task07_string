package tasks;

/**
 * У кожному реченні тексту видалити підрядок максимальної
 * довжини, що починається і закінчується заданими символами.
 */
public class Task11 {

    private Text text = new Text();

    public void taskEleven() {
        text.deleteSubstring();
    }

    public static void main(String[] args) {
        Task11 output = new Task11();
        output.taskEleven();
    }
}
