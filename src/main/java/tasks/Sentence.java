package tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {

  public static final int MAX_AMOUNT_WORDS = 50;
  private String sentence;
  private List<Word> words;
  private List<PunctuationMark> punctuationMarks;

  public Sentence(String sentence) {
    this.sentence = sentence;
    words = new ArrayList<>();
    punctuationMarks = new ArrayList<>();
    divideByWords();
    findPunctuationMarks();

  }


  public String getSentence() {
    return sentence;
  }

  public List<Word> getWords() {
    return words;
  }

  public List<PunctuationMark> getPunctuationMarks() {
    return punctuationMarks;
  }

  public void setSentence(String sentence) {
    this.sentence = sentence;
  }

  private void divideByWords() {
    Pattern p = Pattern.compile("[\\s(-.]+");
    Matcher m = p.matcher(sentence);
    int start = 0;
    while (m.find()) {
      // System.out.println(sentence.substring(start, m.start()));
      words.add(new Word(sentence.substring(start, m.start())));
      start = m.end();
    }
  }

  private void findPunctuationMarks() {
    Pattern p = Pattern.compile("[,.\\-();:\"]");
    Matcher m = p.matcher(sentence);
    while (m.find()) {
      // System.out.println(sentence.charAt( m.start()));
      punctuationMarks.add(new PunctuationMark(sentence.charAt(m.start())));
    }
  }
}
