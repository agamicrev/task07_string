package tasks;

/**
 * Всі слова тексту відсортувати за зростанням
 * кількості заданої букви у слові. Слова з однаковою
 * кількістю розмістити у алфавітному порядку.
 */
public class Task9 {

    private Text text = new Text();

    public void taskNine() {
        text.sortWordAfterInputLetter();
    }

    public static void main(String[] args) {
        Task9 output = new Task9();
        output.taskNine();
    }
}
