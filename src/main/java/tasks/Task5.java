package tasks;

/**
 * У кожному реченні тексту поміняти місцями перше слово, що
 * починається на голосну букву з найдовшим словом.
 */
public class Task5 {

  private Text text = new Text();

  public void taskFive() {
    text.exchangeWords();
  }

  public static void main(String[] args) {
    Task5 output = new Task5();
    output.taskFive();
  }
}
