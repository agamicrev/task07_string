package tasks;

/**
 * У всіх запитальних реченнях тексту знайти і надрукувати
 * без повторів слова заданої довжини.
 */
public class Task4 {

  private Text text = new Text();

  public void taskFour() {
    text.findWordsInQuestions();
  }

  public static void main(String[] args) {
    Task4 output = new Task4();
    output.taskFour();
  }
}
