package tasks;

/**
 * Відсортувати слова у тексті за спаданням кількості входжень
 * заданого символу, а у випадку рівності – за алфавітом.
 */
public class Task13 {
  private Text text = new Text();

  public void taskThirteen() {
    text.sortWordDownAfterInputLetter();
  }

  public static void main(String[] args) {
    Task13 output = new Task13();
    output.taskThirteen();
  }
}
