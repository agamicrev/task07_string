package tasks;

/**
 * Відсортувати слова тексту за зростанням відсотку голосних
 * букв (співвідношення кількості голосних до загальної
 * кількості букв у слові).
 */
public class Task7 {

    private Text text = new Text();

    public void taskSeven() {
        text.sortWordAfterPercentage();
    }

    public static void main(String[] args) {
        Task7 output = new Task7();
        output.taskSeven();
    }

}
