package tasks;

/**
 * Перетворити кожне слово у тексті, видаливши з нього всі
 * наступні (попередні) входження першої (останньої) букви
 * цього слова.
 */
public class Task15 {

  private Text text = new Text();

  public void taskFifteen() {
    text.deleteFirstLetterInWords();
  }

  public static void main(String[] args) {
    Task15 output = new Task15();
    output.taskFifteen();
  }
}
