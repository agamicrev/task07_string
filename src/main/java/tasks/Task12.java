package tasks;

/**
 * З тексту видалити всі слова заданої довжини, що починаються
 * на приголосну букву.
 */
public class Task12 {

    private Text text = new Text();

    public void taskTwelve() {
        text.deleteWords();
    }

    public static void main(String[] args) {
        Task12 output = new Task12();
        output.taskTwelve();
    }
}
