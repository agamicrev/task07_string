package tasks;

import java.util.ArrayList;
import java.util.List;

/**
 * Create class StringUtils with an undefined number of parameters
 * of any class that concatenates all parameters and returns Strings.
 */
public class StringUtils {

  private List<Object> objects = new ArrayList<>();

  public StringUtils addToParameters(Object obj) {
    objects.add(obj);
    return this;
  }

  public String concat() {
    StringBuilder stringBuilder = new StringBuilder();
    for (Object obj : objects) {
      stringBuilder.append(obj).append("  ");
    }
    return stringBuilder.toString();
  }
}
