package tasks;

/**
 * У заданому тексті знайти підрядок максимальної довжини,
 * який є паліндромом, тобто, читається зліва на право і
 * справа на ліво однаково.
 */
public class Task14 {
  private Text text = new Text();

  public void taskFourteen() {
    text.findPolydrome();
  }

  public static void main(String[] args) {
    Task14 output = new Task14();
    output.taskFourteen();
  }

}
