package tasks;

/**
 * Вивести всі речення заданого тексту у порядку зростання
 * кількості слів у кожному з них.
 */
public class Task2 {
  private Text text = new Text();

  public void taskTwo() {
    text.outputSentencesInOrder();
  }

  public static void main(String[] args) {
    Task2 output = new Task2();
    output.taskTwo();
  }
}
