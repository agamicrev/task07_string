package view;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import tasks.StringUtils;

public class MyView {

  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  Locale locale;
  ResourceBundle bundle;

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
    menu.put("7", bundle.getString("7"));
    menu.put("Q", bundle.getString("Q"));
  }

  public MyView() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();

    methodsMenu.put("1", this::testStringUtils);
    methodsMenu.put("2", this::internationalizeMenuUkrainian);
    methodsMenu.put("3", this::internationalizeMenuEnglish);
    methodsMenu.put("4", this::internationalizeMenuPolish);
    methodsMenu.put("5", this::task3);
    methodsMenu.put("6", this::task4);
    methodsMenu.put("7", this::task5);
  }

  private void testStringUtils() {
    StringUtils utils = new StringUtils();
    utils.addToParameters("Andrii")
        .addToParameters("Volodymyrovych")
        .addToParameters("Vertsimaha");
    System.out.println(utils.concat());
  }

  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuPolish() {
    locale = new Locale("pl");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }


  /*
   * Using the documentation for java.util.regex.Pattern as a resource,
   * write and test a regular expression that checks a sentence to see
   * that it begins with a capital letter (велика літера) and ends with a
   * period (крапка).
   */
  private void task3() {
    String text = "If you can use the great opportunity of being here, the result for you will be great.";
    String pattern = "[A-Z]{1}.+\\.";
    Pattern p = Pattern.compile(pattern);
    Matcher m = p.matcher(text);

    System.out.println("STRING: \n" + text);

    if (m.matches()) {
      System.out.println("Begins with a capital, ends with period");
    } else {
      System.out.println("Doesn't meet the requirements");
    }

  }


  /*
   * Split some string on the words "the" or "you".
   */
  private void task4() {
    String text = "If you can use the great opportunity of being here, the result for you will be great.";
    System.out.println("STRING: \n" + text);

    System.out.println("STRING AFTER SPLIT: ");

    String[] splitTxt = text.split
        ("[  \\t\\n\\x0B\\f\\r]you[  \\t\\n\\x0B\\f\\r]|[  \\t\\n\\x0B\\f\\r]the[  \\t\\n\\x0B\\f\\r]");
    for (String str : splitTxt) {
      System.out.println(str);
    }
  }

  /*
   * Replace all the vowels(голосні) in some text with underscores(_).
   */
  private void task5() {
    String text = "If you can use the great opportunity of being here, the result for you will be great.";
    System.out.println("STRING: \n" + text);
    System.out.println("STRING AFTER REPLACE VOWELS WITH \"_\":");
    String text2 = text
        .replaceAll("[AaEeIiJjOoYyUu]", "_");
    System.out.println(text2);
  }

  //-------------------------------------------------------------------------

  private void outputMenu() {
    System.out.println("\n*********** MENU ***********");
    for (String key : menu.keySet()) {
      if (key.length() == 1) {
        System.out.println(menu.get(key));
      }
    }
  }

  public void show() {
    String keyMenu = " ";
    do {
      if (!keyMenu.equals("Q")) {
        outputMenu();
        System.out.println("Please, select menu point: ");
        keyMenu = input.nextLine().toUpperCase();
        try {
          methodsMenu.get(keyMenu).print();
        } catch (Exception e) {
        }
      }
    } while (!keyMenu.equals("Q"));
  }
}


